# Kalepso
Kalepso is a Java library that offers you to securly store files and databases while retaining search and retrieval functionality. Using this library, no one will be able to infer anything about your data, while you keep all the functionality like searching, retrieving or updating your data. The technology we use ensures that no current or future attacks can compromise your data privacy.
The library is cloud agnostic, meaning that it doesn't matter what cloud you want to use.

# Documentation
[link to API]

# Download and Dependencies
In order to use Kalepso, you just need Java 8 and three files: 

1. Kalepso.jar, which has dependencies to the following two files

2. Spring Security Crypto 5.0.0: https://mvnrepository.com/artifact/org.springframework.security/spring-security-crypto

3. Colt 1.2.0: https://mvnrepository.com/artifact/colt/colt

All you need to do is to download these three jar files and include them into your java project. 

# Limitations
The Kalepso library currently has the following limitations:

1. Maximum number of records: 16384

2. Maximum number of tables: 10

3. Maximum number of index updates: 256

4. Maximum record size: 512 byte

# Basic Usage
Before using the Kalepso library, you need to connect to our server using the ip address provided by us.
```java
	    //connect to Kalepso
	    Kalepso kalepso = Kalepso.getInstance();
	    kalepso.connect("IP_ADDRESS");
```
Once connected, you can create a new table in your database. For each column of the table, a new TableStructure object has to be created which specifies the attribute name and its datatype. All TableStructure objects have to be added to an ArrayList with which the createTable() function can be called in order to create the new table.
```java
        //create a new table
        ArrayList<TableStructure> tableStructure = new ArrayList<>();
        tableStructure.add(new TableStructure("Name", java.lang.String.class));
        tableStructure.add(new TableStructure("Age", java.lang.Integer.class));
        tableStructure.add(new TableStructure("Salary", java.lang.Integer.class));
        tableStructure.add(new TableStructure("Married", java.lang.Boolean.class));
        kalepso.createTable("exampleTable", tableStructure);
```
Now that we have a table in the database, we can index attributes that we want to search for later on. In order to index an attribute of number datatype(integer, double, float...), we need to create a RangeMetadata object and add it to a HashMap together with the attribute name. Then we can call the createRangeIndex() function, which creates indices for all attributes in the HashMap.
The indexing works similar for String and Boolean attributes. For Boolean attributes and String attributes with a predefined list of values, we create a PointMetadata object, which specifies the amount and the values of the predefined elements. All of these attributes are added to a HashMap together with the PointMetadata object. Having this HashMap, we can then call the createFixedPointIndex() function in order to Index the attributes.
For String with no predefined values, we can just create an ArrayList with all attribute names that should be indexed and then call the createPointIndex() function with this ArrayList.
```java
      //index the attributes you want to search for
        HashMap<String, RangeMetadata> attributes = new HashMap<>();
        RangeMetadata ageParams = new RangeMetadata(0, 100, 1);
        attributes.put("Age", ageParams);
        RangeMetadata salaryParams = new RangeMetadata(0, 500000, 1000);
        attributes.put("Salary", salaryParams);
        kalepso.createRangeIndex(attributes, "exampleTable");


        PointMetadata pMeta = new PointMetadata(new ArrayList<String>(Arrays.asList(new String[]{"true","false"})));
        HashMap<String, PointMetadata> allPMeta = new HashMap<>();
        allPMeta.put("Married", pMeta);
        kalepso.createFixedPointIndex(allPMeta, "exampleTable");
        ArrayList<String> stringAtts = new ArrayList<>();
        stringAtts.add("Name");
        kalepso.createPointIndex(stringAtts, "exampleTable");
```
Having created the table and the indices, we can now add records to the table. A record is an Object array, which contains a value for each column. All records can be added to an ArrayList, which can then be added to the table by using the add() function.
```java
        //add data to your table
        ArrayList<Object[]> records = new ArrayList<>();
        Object[] row1 = {"exampleName1", 20, 300000, true};
        Object[] row2 = {"exampleName2", 50, 400000, false};
        Object[] row3 = {"exampleName3", 40, 800000, true};

        records.add(row1);
        records.add(row2);
        records.add(row3);
        kalepso.add(records, "exampleTable");
```
We can now create and perform queries. In order to create a query for an attribute of number datatype (integer, double, float...), we can call the rangeQuery() function and specify the range of values, we want to search for.
To search for a Boolean or String attribute with predefined values, we can call the fixedPointQuery() function and specify the attribute name and the value we want to search for.
The same can be done for String attributes without predefined values by calling the pointQuery() function.
All query objects can be added to an ArrayList. If the issueQuery() function is called with this ArrayList, all queries will be performed at once. The matching records will be stored in an ArrayList of Object arrays.
```java
	            //create query: return all records with Age from 20 to 70, Salary from 100000 to 400000, Married = true and Name = exampleName1
        ArrayList<Query> query = new ArrayList<>();
        query.add(kalepso.rangeQuery("Age", 20, 70, "exampleTable"));
        query.add(kalepso.rangeQuery("Salary", 200000, 300000, "exampleTable"));
        query.add(kalepso.fixedPointQuery("Married", "true", "exampleTable"));
        query.add(kalepso.pointQuery("Name", "exampleName1", "exampleTable"));

        //execute query
        HashMap<String, ArrayList<Object[]>> result = (HashMap)kalepso.issueQuery(query);

        //get all table names
        ArrayList<String> tableList = new ArrayList<String>(result.keySet());


        //print retrieved records
            for(int k = 0; k < tableList.size(); k++)
            {
                System.out.println("----------"+tableList.get(k)+"----------");
                for(int i = 0; i < result.get(tableList.get(k)).size(); i++)
                {
                    for(int j = 0; j < result.get(tableList.get(k)).get(i).length; j++)
                    {
                        System.out.print(result.get(tableList.get(k)).get(i)[j] + " ");
                    }
                    System.out.println();
                }
            }

```
After everything is done, you can disconnect from Kalepso.
```java
		kalepso.disconnect();
```

#### Remarks
1. In order to delete a record from a table, the deleteRecord() function can be called. This function takes as input the ID of the record and the name of the table which contains the record. Whenever a new record is added to a table, it is assigned a unique ID, which is set as the first entry of each record. This ID is required in order to delete or update a record.

2. When you disconnect from Kalepso, all structures that you created will be lost. In order to save these structures, you can call the saveSession() function. This function takes a String as input, specifying the name of the session. If you want to continue with this session at a later point, you can call the loadSession() function, giving the name of the session as input that you want to resume from.